import * as React from 'react';
import Ticket from '../../../../entities/domain-model/ticket/ticket';

interface Props {
    ticket: Ticket;
}

export default class extends React.Component<Props> {

    constructor(props: any) {
        super(props);
    }

    render() {
        const ticket = this.props.ticket;

        return (
            <div className="a-tickets">
                <div className="a-tickets__ticket">
                    <div className="a-tickets__ticket-row">
                        <div className="a-tickets__ticket-event">{ticket.eventName}</div>
                        <div className="a-tickets__ticket-actions">
                            <div className="a-vip-icon a-vip-icon_position hidden"/>
                            <div className="a-vip-icon a-vip-icon_zip"/>
                        </div>
                    </div>
                    <div className="a-tickets__ticket-row">
                        <div className="a-tickets__ticket-outcome">
                            {ticket.outcomeName}
                        </div>
                        <div className="a-tickets__ticket-price">
                            {ticket.priceFormatted}
                        </div>
                        <div className="a-tickets__ticket-amount">
                            {ticket.amountFormatted}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
