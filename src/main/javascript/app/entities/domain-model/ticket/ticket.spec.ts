import Ticket from './ticket';
import Bet from '../bet/bet';
import TotalMarket from '../markets/total-market/total-market';
import LeagueEvent from '../league-event/league-event';

describe('Ticket', () => {
    it('should create default instance if there are no constructor parameters', () => {
        const ticket = new Ticket();

        expect(ticket.id).toEqual(0);
        expect(ticket.priceFormatted).toEqual('');
        expect(ticket.amountFormatted).toEqual('');
        expect(ticket.eventName).toEqual('');
        expect(ticket.outcomeName).toEqual('');
    });

    it('should parse raw data', () => {
        const ticket = new Ticket({
            id: 71326,
            eventName: 'Tottenham Hotspur - West Ham United',
            outcomeName: 'home',
            priceFormatted: 1.42,
            amountFormatted: 50000
        });

        expect(ticket.id).toEqual(71326);
        expect(ticket.priceFormatted).toEqual(1.42);
        expect(ticket.amountFormatted).toEqual(50000);
        expect(ticket.eventName).toEqual('Tottenham Hotspur - West Ham United');
        expect(ticket.outcomeName).toEqual('home');
    });

    it('should create ticket from bet with outcomeB outcome', () => {
        const bet = new Bet({
            id: '66765',
            marketId: '10901',
            amount: '500000',
            customerId: '177',
            price: '7.77',
            type: 'under',
            typeValue: 'some value'
        });

        const market = new TotalMarket({
            id: 47371,
            periodId: 47878,
            points: 2.50,
            over: 2.11,
            under: 1.80
        });

        const event = new LeagueEvent({
            id: 1188,
            startDate: 1513108800000,
            home: 'Huddersfield Town',
            away: 'Chelsea',
            periods: []
        });

        const ticket = Ticket.createTicketFromBet(bet, market, event);

        expect(ticket.id).toEqual(66765);
        expect(ticket.eventName).toEqual('Huddersfield Town - Chelsea');
        expect(ticket.outcomeName).toEqual('Under 2.50');
        expect(ticket.priceFormatted).toEqual('7.77');
        expect(ticket.amountFormatted).toEqual('500 000');
    });

    it('should create ticket from bet with outcomeA outcome', () => {
        const bet = new Bet({
            id: '66765',
            marketId: '47371',
            amount: '500000',
            customerId: '177',
            price: '7.77',
            type: 'over',
            typeValue: 'some value'
        });

        const market = new TotalMarket({
            id: 47371,
            periodId: 47878,
            points: 2.50,
            over: 2.11,
            under: 1.80
        });

        const event = new LeagueEvent({
            id: 1188,
            startDate: 1513108800000,
            home: 'Huddersfield Town',
            away: 'Chelsea',
            periods: []
        });

        const ticket = Ticket.createTicketFromBet(bet, market, event);

        expect(ticket.id).toEqual(66765);
        expect(ticket.eventName).toEqual('Huddersfield Town - Chelsea');
        expect(ticket.outcomeName).toEqual('Over 2.50');
        expect(ticket.priceFormatted).toEqual('7.77');
        expect(ticket.amountFormatted).toEqual('500 000');
    });
});