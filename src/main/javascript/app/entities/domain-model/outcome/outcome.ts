import LeagueEvent from '../league-event/league-event';
import Market from '../markets/common/market';

export default class Outcome {
    name: string;
    event: LeagueEvent;
    market: Market;
    outcomeType: string;

    constructor(data?: any) {
        data = data || {};

        this.event = new LeagueEvent(data.event);
        this.market = data.market;
        this.outcomeType = data.outcomeType || '';
        this.name = data.name || this.market ? this.market.getOutcomeName(this.outcomeType, this.event) : '';
    }
}