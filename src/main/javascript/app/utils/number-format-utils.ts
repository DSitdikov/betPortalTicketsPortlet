export default class NumberFormatUtils {

    static isolateThousands(value: number) {
        return value.toString()
            .replace(/\D/g, '')
            .replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    }
}
