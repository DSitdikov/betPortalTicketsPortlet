import * as React from 'react';
import classNames from 'classnames';
import { connect, Dispatch } from 'react-redux';
import CommonAction from '../../actions/common/types/common-action';
import { Action } from 'redux';
import env from '../../env/env';
import { closeNotification } from '../../actions/ui/common/common.actions';

interface DispathToPropsType {
    onClose: () => Action;
}

interface Props extends DispathToPropsType {
    title: string;
    message: string;
    mode: string;
    show: boolean;
}

class Notification extends React.Component<Props> {
    private closeNotificationTimeout;

    constructor(props: Props) {
        super(props);
    }

    handleKeyPress(event: any) {
        if (event.key === 'Esc') {
            this.props.onClose();
        }
    }

    componentWillReceiveProps(nextProps: Props) {
        if (nextProps.show) {
            this.closeNotificationTimeout = setTimeout(() => this.props.onClose(), env.closeTimeout);
        }
    }

    handleCloseNotification() {
        this.props.onClose();
        clearTimeout(this.closeNotificationTimeout);
    }

    render() {
        const { title, message, mode, show } = this.props;

        const notificationStyle = classNames({
            'a-notification': true,
            'a-notification_hidden': !show
        });

        return (
            <div className={notificationStyle} onKeyPress={e => this.handleKeyPress(e)}>
                <div className="a-notification__left-side"/>
                <div className={`a-notification__bar a-notification__bar_mode_${mode}`}>
                    <div className="a-notification__title">{title}</div>
                    <div className="a-notification__message">{message}</div>
                    <div className="a-notification__btn-close" onClick={() => this.handleCloseNotification()}>
                        <div className="a-vip-icon a-vip-icon_close"/>
                    </div>
                </div>
                <div className="a-notification__right-side"/>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        onClose: () => dispatch(closeNotification())
    };
}

export default connect(null, mapDispatchToProps)(Notification);