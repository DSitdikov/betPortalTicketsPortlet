import * as React from 'react';
import { connect } from 'react-redux';
import StoreState from '../../../entities/store-state';
import Scrollbars from 'react-custom-scrollbars-patched';
import SportSwitcher from './sport-switcher/sport-switcher';
import League from '../../../entities/domain-model/league/league';
import LeagueView from './league-view/league-view';
import ScrollComponent from '../../common/scroll-component';

interface StateToPropsType {
    leagues: Map<number, League>;
}

class LeagueSidebar extends ScrollComponent<StateToPropsType> {

    constructor(props: StateToPropsType) {
        super(props);
    }

    render() {
        const leagueIds = Array.from(this.props.leagues.values());

        return (
            <div className="a-sidebar-left">
                <SportSwitcher/>
                <div className="a-sidebar-left__leagues">
                    <Scrollbars
                        className="a-scrollbar"
                        autoHide={true}
                        browserScrollbarWidth={this.state.scrollbarWidth}
                        renderView={props => <div {...props} className="a-scrollbar__container"/>}
                        renderThumbVertical={props => <div {...props} className="a-scrollbar__thumb-vertical"/>}
                    >
                        {leagueIds.map(league => (
                            <LeagueView key={league.id} league={league}/>
                        ))}
                    </Scrollbars>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        leagues: state.entities.leagues
    };
}

export default connect(mapStateToProps)(LeagueSidebar);
