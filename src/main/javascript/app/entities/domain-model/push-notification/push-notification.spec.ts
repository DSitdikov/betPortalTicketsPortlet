import PushNotification from './push-notification';
import { PushActionType } from '../../../services/push-processing/common/push-action-type';
import { PushEntityType } from '../../../services/push-processing/common/push-entity-type';

describe('PushNotification', () => {
    it('should create default instance if there are no constructor arguments', () => {
        const notification = new PushNotification();

        expect(notification.actionType).toEqual('');
        expect(notification.entityType).toEqual('');
        expect(notification.data).toEqual({});
    });

    it('should parse raw push notification', () => {
        const notification = new PushNotification({
            actionType: PushActionType.UPDATE,
            entityType: PushEntityType.MARKET,
            data: {
                id: 47376,
                pinnacleLineId: 450840664,
                away: 1.34,
                home: 10.51,
                draw: 5.22
            }
        });

        expect(notification.actionType).toEqual('update');
        expect(notification.entityType).toEqual('market');
        expect(notification.data).toEqual({
            id: 47376,
            pinnacleLineId: 450840664,
            away: 1.34,
            home: 10.51,
            draw: 5.22
        });
    });
});