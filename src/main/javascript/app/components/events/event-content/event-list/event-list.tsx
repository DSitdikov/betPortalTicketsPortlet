import StoreState from '../../../../entities/store-state';
import { connect } from 'react-redux';
import * as React from 'react';
import { EventListBlurredItem } from './event-list-blurred-item/event-list-blurred-item';
import { EventListItem } from './event-list-item/event-list-item';
import EventUiItem from '../../../../entities/ui/event-ui-state/event-ui-state';
import LeagueEvent from '../../../../entities/domain-model/league-event/league-event';
import Scrollbars from 'react-custom-scrollbars-patched';
import ScrollComponent from '../../../common/scroll-component';

interface StateToPropsType {
    events: Map<number, LeagueEvent>;
    eventUiState: Map<number, EventUiItem>;
    busy: boolean;
}

type Props = StateToPropsType;

class EventList extends ScrollComponent<Props> {

    constructor(props: Props) {
        super(props);
    }

    get eventsPublished() {
        const events = Array.from(this.props.events.values());
        const eventUiState = this.props.eventUiState;
        return events.filter(event => (eventUiState.get(event.id) || new EventUiItem()).selected);
    }

    render() {
        return (
            <div className="a-events">
                <Scrollbars
                    className="a-scrollbar"
                    autoHide={true}
                    browserScrollbarWidth={this.state.scrollbarWidth}
                    renderView={props => <div {...props} className="a-scrollbar__container"/>}
                    renderThumbVertical={props => <div {...props} className="a-scrollbar__thumb-vertical"/>}
                >
                    <div className={this.props.busy ? 'hidden' : ''}>
                        {this.eventsPublished.map(event => (
                            <EventListItem key={event.id} event={event}/>
                        ))}
                    </div>
                    <div className={this.props.busy ? '' : 'hidden'}>
                        <EventListBlurredItem/>
                        <EventListBlurredItem/>
                        <EventListBlurredItem/>
                        <EventListBlurredItem/>
                    </div>
                </Scrollbars>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        events: state.entities.events,
        eventUiState: state.ui.events,
        busy: state.ui.common.busy
    };
}

export default connect(mapStateToProps)(EventList);
