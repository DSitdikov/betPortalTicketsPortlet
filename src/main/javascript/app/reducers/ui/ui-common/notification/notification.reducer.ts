import CommonAction from '../../../../actions/common/types/common-action';
import AppNotification from '../../../../entities/ui/app-notification/app-notification';
import { UiActionType } from '../../../../actions/ui/ui-action-type';

export default function notificationReducer(state: AppNotification | null = null, action: CommonAction) {
    switch (action.type) {
        case UiActionType.SHOW_NOTIFICATION:
            return new AppNotification(action.payload);
        case UiActionType.CLOSE_NOTIFICATION:
            return null;
        default:
            return state;
    }
}