import Outcome from './outcome';
import LeagueEvent from '../league-event/league-event';
import TotalMarket from '../markets/total-market/total-market';
import HandicapMarket from '../markets/handicap-market/handicap-market';
import MoneyLineMarket from '../markets/money-line-market/money-line-market';

describe('Outcome', () => {
    it('should create default instance if there are no constructor parameters', () => {
        const outcome = new Outcome();
        const event = new LeagueEvent();

        expect(outcome.name).toEqual('');
        expect(outcome.market).toEqual(undefined);
        expect(outcome.event.id).toEqual(event.id);
        expect(outcome.outcomeType).toEqual('');
    });

    it('should create outcome with money line market', () => {
        const outcomeData = {
            event: new LeagueEvent({
                home: 'Huddersfield Town',
                away: 'Chelsea'
            }),
            market: new MoneyLineMarket(),
            outcomeType: 'away'
        };

        const outcome = new Outcome(outcomeData);
        expect(outcome.name).toEqual('Chelsea ML');
        expect(outcome.event.id).toEqual(0);
    });

    it('should create outcome with total market', () => {
        const outcomeData = {
            event: new LeagueEvent(),
            market: new TotalMarket({ points: 2.5 }),
            outcomeType: 'under'
        };

        const outcome = new Outcome(outcomeData);
        expect(outcome.name).toEqual('Under 2.50');
        expect(outcome.event.id).toEqual(0);
    });

    it('should create outcome with handicap market', () => {
        const outcomeData = {
            event: new LeagueEvent({
                home: 'Huddersfield Town',
                away: 'Chelsea'
            }),
            market: new HandicapMarket({ hdp: 1.6, }),
            outcomeType: 'home'
        };

        const outcome = new Outcome(outcomeData);
        expect(outcome.name).toEqual('Huddersfield Town +1.60');
        expect(outcome.event.id).toEqual(0);
    });
});
