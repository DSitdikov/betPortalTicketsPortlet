import * as React from 'react';
import Events from '../events/events';
import Navbar from '../common/navbar';
import { Route, Redirect, Switch } from 'react-router-dom';

interface Props {
}

export default class ClientArea extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Navbar/>
                <Switch>
                    <Route path="/client/events" component={Events}/>
                    <Redirect strict={true} from="/client" to="/client/events"/>
                </Switch>
            </div>
        );
    }
}