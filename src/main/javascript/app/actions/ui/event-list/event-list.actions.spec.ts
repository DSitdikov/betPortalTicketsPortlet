import { selectMarket } from './event-list.actions';
import configureMockStore from 'redux-mock-store';
import thunk, { ThunkAction } from 'redux-thunk';
import Market from '../../../entities/domain-model/markets/common/market';
import LeagueEventPeriod from '../../../entities/domain-model/league-event-period/league-event-period';
import PeriodUiState from '../../../entities/ui/period-ui-state/period-ui-state';
import CommonAction from '../../common/types/common-action';
import { MarketType } from '../../../entities/domain-model/markets/common/market-type';
import { TotalType } from '../../../entities/domain-model/markets/total-market/total-type';
import TotalMarket from '../../../entities/domain-model/markets/total-market/total-market';
import StoreState from '../../../entities/store-state';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('EventListActions', () => {
    describe('selectMarket', () => {
        let store;

        beforeEach(() => {
            store = {
                entities: {
                    periods: new Map<number, LeagueEventPeriod>(),
                    markets: new Map<number, Market>()
                },
                ui: {
                    periods: new Map<number, PeriodUiState>()
                }
            };
        });

        it('should return market sibling ids of overall totals type if it selected as current', () => {
            store.entities.periods.set(0, new LeagueEventPeriod({
                id: 0,
                totals: [{ id: 1 }, { id: 2 }, { id: 3 }]
            }));

            for (let i = 0; i < 10; ++i) {
                store.entities.markets.set(i, new TotalMarket({
                    id: i,
                    periodId: 0,
                    type: MarketType.TOTALS
                }));
            }

            store.ui.periods.set(0, new PeriodUiState({
                selectedTotalsType: TotalType.OVERALL
            }));

            const mockedStore = mockStore(store);
            const market = selectMarket(0) as ThunkAction<CommonAction, StoreState, void>;
            const payload = mockedStore.dispatch(market).payload;
            expect(payload.marketSiblingIds).toEqual([1, 2, 3]);
        });

        it('should return market sibling ids of away totals type if it selected as current', () => {
            store.entities.periods.set(0, new LeagueEventPeriod({
                id: 0,
                teamTotals: [{ id: 1 }, { id: 2 }, { id: 3 }]
            }));

            for (let i = -3; i < 4; ++i) {
                store.entities.markets.set(i, new TotalMarket({
                    id: i,
                    periodId: 0,
                    type: MarketType.TOTALS
                }));
            }

            store.ui.periods.set(0, new PeriodUiState({
                selectedTotalsType: TotalType.AWAY
            }));

            const mockedStore = mockStore(store);
            const market = selectMarket(0) as ThunkAction<CommonAction, StoreState, void>;
            const payload = mockedStore.dispatch(market).payload;
            expect(payload.marketSiblingIds).toEqual([-1, -2, -3]);
        });

        it('should return market sibling ids of home totals type if it selected as current', () => {
            store.entities.periods.set(0, new LeagueEventPeriod({
                id: 0,
                teamTotals: [{ id: 1 }, { id: 2 }, { id: 3 }]
            }));

            for (let i = -3; i < 4; ++i) {
                store.entities.markets.set(i, new TotalMarket({
                    id: i,
                    periodId: 0,
                    type: MarketType.TOTALS
                }));
            }

            store.ui.periods.set(0, new PeriodUiState({
                selectedTotalsType: TotalType.HOME
            }));

            const mockedStore = mockStore(store);
            const market = selectMarket(0) as ThunkAction<CommonAction, StoreState, void>;
            const payload = mockedStore.dispatch(market).payload;
            expect(payload.marketSiblingIds).toEqual([1, 2, 3]);
        });

        it('should return market sibling ids of overall totals type in spite of it is undefined ', () => {
            store.entities.periods.set(0, new LeagueEventPeriod({
                id: 0,
                totals: [{ id: 1 }, { id: 2 }, { id: 3 }]
            }));

            for (let i = 0; i < 4; ++i) {
                store.entities.markets.set(i, new TotalMarket({
                    id: i,
                    periodId: 0,
                    type: MarketType.TOTALS
                }));
            }

            store.ui.periods.set(0, new PeriodUiState());

            const mockedStore = mockStore(store);
            const market = selectMarket(0) as ThunkAction<CommonAction, StoreState, void>;
            const payload = mockedStore.dispatch(market).payload;
            expect(payload.marketSiblingIds).toEqual([1, 2, 3]);
        });

        it('should return empty list of totals market sibling ids if there are no totals', () => {
            store.entities.periods.set(0, new LeagueEventPeriod({
                id: 0,
            }));

            store.entities.markets.set(0, new TotalMarket({
                id: 0,
                periodId: 0,
                type: MarketType.TOTALS
            }));

            store.ui.periods.set(0, new PeriodUiState({
                selectedTotalsType: TotalType.OVERALL
            }));

            const mockedStore = mockStore(store);
            const market = selectMarket(0) as ThunkAction<CommonAction, StoreState, void>;
            const payload = mockedStore.dispatch(market).payload;
            expect(payload.marketSiblingIds).toEqual([]);
        });
    });
});
