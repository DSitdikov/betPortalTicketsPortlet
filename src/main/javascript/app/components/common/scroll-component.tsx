import * as React from 'react';

interface StateProps {
    scrollbarWidth: number;
}

export default abstract class ScrollComponent<P> extends React.Component<P, StateProps> {
    private static SCROLLBAR_WIDTH_UPDATE_PERIOD = 300;
    // Patch for 60% zooming on Chrome
    private static SCROLLBAR_ADDITION_SHIFTING = 1;

    constructor(props: any) {
        super(props);

        this.state = {
            scrollbarWidth: this.getScrollbarWidth()
        };

        this.updateScrollbarWidth();
    }

    updateScrollbarWidth() {
        setTimeout(() => {
            const newScrollbarWidth = this.getScrollbarWidth();

            if (newScrollbarWidth !== this.state.scrollbarWidth) {
                this.setState({
                    scrollbarWidth: this.getScrollbarWidth()
                });
            }

            this.updateScrollbarWidth();
        }, ScrollComponent.SCROLLBAR_WIDTH_UPDATE_PERIOD);
    }

    // noinspection JSMethodCanBeStatic
    getScrollbarWidth() {
        const outer = document.createElement('div');
        outer.style.visibility = 'hidden';
        outer.style.width = '100px';
        outer.style.msOverflowStyle = 'scrollbar';

        document.body.appendChild(outer);

        const widthNoScroll = outer.offsetWidth;

        outer.style.overflow = 'scroll';

        const inner = document.createElement('div');
        inner.style.width = '100%';
        outer.appendChild(inner);

        const widthWithScroll = inner.offsetWidth;

        if (outer.parentNode) {
            outer.parentNode.removeChild(outer);
        }

        return widthNoScroll - widthWithScroll + ScrollComponent.SCROLLBAR_ADDITION_SHIFTING;
    }
}
