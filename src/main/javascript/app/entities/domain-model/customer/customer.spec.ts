import Customer from './customer';

describe('Customer', () => {
    it('should create default instance if there are no constructor parameters', () => {
        const customer = new Customer();

        expect(customer.id).toEqual('');
        expect(customer.login).toEqual('');
    });

    it('should correctly parse raw customer data', () => {
        const customer = new Customer({
            id: 'F901',
            login: 'F901 (login)'
        });

        expect(customer.id).toEqual('F901');
        expect(customer.login).toEqual('F901 (login)');
    });
});