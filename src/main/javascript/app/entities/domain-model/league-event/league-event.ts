import { format } from 'fecha';
import { PeriodType } from '../league-event-period/period-type';
import DateFormatUtils from '../../../utils/date-format-utils/date-format-utils';

export default class LeagueEvent {
    id: number;
    leagueId: number;
    name: string;
    hasValidDate: boolean;
    startDateOrigin: Date;
    startDate: Date;
    startTimeFormatted: string;
    startDateFormatted: string;
    startIn: string;
    home: string;
    away: string;
    fullTimePeriod: number;
    periods: number[];

    constructor(data?: any) {
        data = data || {};

        this.id = data.id || 0;
        this.leagueId = data.leagueId || 0;

        let startDate = data.startDateOrigin || data.startDate;
        if (typeof startDate === 'string') {
            startDate = new Date(startDate).getTime();
        }
        startDate = startDate instanceof Date ? startDate.getTime() : startDate;
        this.startDateOrigin = new Date(startDate);
        this.hasValidDate = !isNaN(this.startDateOrigin.getTime());

        // Cut off time zone to display date in UTC 0 instead of date in special user timezone
        // Timezone offset is in minutes (for example, +03:00 -> -180 minutes)
        this.startDate = new Date(startDate + this.startDateOrigin.getTimezoneOffset() * 60 * 1000);
        // Format date
        this.startTimeFormatted = this.hasValidDate ? format(this.startDate, 'HH:mm') : '';
        // Format: September 9, 2017, 18:40
        this.startDateFormatted = this.hasValidDate ? format(this.startDate, 'MMMM D, YYYY, HH:mm') : '';

        let dateDifference = this.startDateOrigin.getTime() - Date.now();
        if (dateDifference < 0) {
            dateDifference = 0;
        }
        this.startIn = DateFormatUtils.formatDateDifference(dateDifference);

        this.home = data.home || '';
        this.away = data.away || '';
        this.name = this.home && this.away ? `${this.home} - ${this.away}` : '';

        const periods = data.periods || [];
        this.periods = periods.map(period => period.id);

        const fullTimePeriod = periods.find(period => period.number === PeriodType.FULL_TIME);
        this.fullTimePeriod = (fullTimePeriod || {}).id || 0;
    }
}
