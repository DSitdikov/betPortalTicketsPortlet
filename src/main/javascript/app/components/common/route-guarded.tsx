import * as React from 'react';
import StoreState from '../../entities/store-state';
import { connect } from 'react-redux';
import { Redirect, Route, withRouter } from 'react-router-dom';

interface Props {
    component: any;
    path: string;
}

interface StateToPropsType {
    logged?: boolean;
}

type PropsToType = Props & StateToPropsType;

class RouteGuarded extends React.Component<PropsToType> {

    constructor(props: PropsToType) {
        super(props);
    }

    render() {
        const props: Props = this.props;
        const pathname = window.location.pathname;

        if (!this.props.logged) {
            return (
                <Redirect to={{ pathname: '/login', state: { from: pathname } }}/>
            );
        }

        return (
            <Route {...props}/>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        logged: state.entities.authentication.logged
    };
}

export default withRouter(connect(mapStateToProps)(RouteGuarded));
