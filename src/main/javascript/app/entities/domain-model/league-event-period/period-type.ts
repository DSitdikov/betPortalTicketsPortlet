export const PeriodType = {
    FULL_TIME: 0,
    FIRST_HALF: 1,
    SECOND_HALF: 2
};