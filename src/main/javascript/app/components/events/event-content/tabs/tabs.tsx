import * as React from 'react';
import { connect } from 'react-redux';
import StoreState from '../../../../entities/store-state';
import LeagueEvent from '../../../../entities/domain-model/league-event/league-event';
import EventUiState from '../../../../entities/ui/event-ui-state/event-ui-state';

interface StateToPropsType {
    events: Map<number, LeagueEvent>;
    eventUiState: Map<number, EventUiState>;
}

type Props = StateToPropsType;

class Tabs extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    get eventsPublished() {
        const events = Array.from(this.props.events.values());
        const eventUiState = this.props.eventUiState;
        return events.filter(event => (eventUiState.get(event.id) || new EventUiState()).selected);
    }

    render() {
        return (
            <div className="a-tabs">
                <div className="a-tabs__item a-tabs__item_selected">
                    <div className="a-tabs__item-title">Prematch</div>
                    <div className="a-tabs__item-counter">{this.eventsPublished.length}</div>
                </div>
                <div className="a-tabs__item"/>
                <div className="a-tabs__item"/>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        events: state.entities.events,
        eventUiState: state.ui.events
    };
}

export default connect(mapStateToProps)(Tabs);
