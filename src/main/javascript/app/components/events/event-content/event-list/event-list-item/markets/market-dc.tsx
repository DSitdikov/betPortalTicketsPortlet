import * as React from 'react';
import FastDealData from '../../../../../../entities/ui/modals/fast-deal/fast-deal-data';
import { OutcomeType } from '../../../../../../entities/domain-model/outcome/outcome-type';
import { Action } from 'redux';
import LeagueEvent from '../../../../../../entities/domain-model/league-event/league-event';
import { connect, Dispatch } from 'react-redux';
import CommonAction from '../../../../../../actions/common/types/common-action';
import { selectLine } from '../../../../../../actions';
import DoubleChanceMarket
    from '../../../../../../entities/domain-model/markets/double-chance-market/double-chance-market';

interface DispathToPropsType {
    onSelectLine: (data: FastDealData) => Action;
}

interface Props extends DispathToPropsType {
    event: LeagueEvent;
}

class MarketDc extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        const outcomeNoLineClass = 'a-event-market__outcome-value a-event-market__outcome-value_type_no-line';
        const outcomeHasValueClass = 'a-event-market__outcome-value';

        const event = this.props.event;
        const doubleChance = new DoubleChanceMarket();

        return (
            <div className="a-event-market a-event-market_type_dc">
                <div className="a-event-market__title">DC</div>
                <div className="a-event-market__line">
                    <div className="a-event-market__outcome-subtitle">X2</div>
                    <div
                        onClick={() => {
                            this.props.onSelectLine(new FastDealData({
                                marketId: doubleChance.originId,
                                price: doubleChance.outcomeA,
                                event: event.id,
                                outcomeName: `X2 (${event.home} ML or Draw)`,
                                outcomeType: OutcomeType.HOME
                            }));
                        }}
                        className={doubleChance.outcomeA.hasValue ? outcomeHasValueClass : outcomeNoLineClass}
                    >
                        {doubleChance.outcomeA.formatted}
                    </div>
                </div>
                <div className="a-event-market__line">
                    <div className="a-event-market__outcome-subtitle">12</div>
                    <div
                        onClick={() => {
                            this.props.onSelectLine(new FastDealData({
                                marketId: doubleChance.originId,
                                price: doubleChance.draw,
                                event: event.id,
                                outcomeName: `12 (${event.home} ML or ${event.away} ML)`,
                                outcomeType: OutcomeType.DRAW
                            }));
                        }}
                        className={doubleChance.draw.hasValue ? outcomeHasValueClass : outcomeNoLineClass}
                    >
                        {doubleChance.draw.formatted}
                    </div>
                </div>
                <div className="a-event-market__line">
                    <div className="a-event-market__outcome-subtitle">X2</div>
                    <div
                        onClick={() => {
                            this.props.onSelectLine(new FastDealData({
                                marketId: doubleChance.originId,
                                price: doubleChance.outcomeB,
                                event: event.id,
                                outcomeName: `1x (${event.away} ML or Draw)`,
                                outcomeType: OutcomeType.AWAY
                            }));
                        }}
                        className={doubleChance.outcomeB.hasValue ? outcomeHasValueClass : outcomeNoLineClass}
                    >
                        {doubleChance.outcomeB.formatted}
                    </div>
                </div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        onSelectLine: (data: FastDealData) => dispatch<any>(selectLine(data))
    };
}

export default connect(null, mapDispatchToProps)(MarketDc);
