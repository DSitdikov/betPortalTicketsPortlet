import HandicapMarket from './handicap-market';
import { OutcomeType } from '../../outcome/outcome-type';
import LeagueEvent from '../../league-event/league-event';

describe('HandicapMarket', () => {
    it('should create default instance if there are no constructor parameters', () => {
        const market = new HandicapMarket();

        expect(market.id).toEqual(0);
        expect(market.originId).toEqual(0);
        expect(market.periodId).toEqual(0);
        expect(market.type).toEqual('Handicap');
        expect(market.outcomeA.hasValue).toEqual(false);
        expect(market.outcomeB.hasValue).toEqual(false);
        expect(market.hdp).toEqual(0);
    });

    it('should parse raw handicap and set price tips', () => {
        const market = new HandicapMarket({
            id: 47370,
            periodId: 47551,
            away: 1.40,
            home: 3.01,
            hdp: 0.6
        });

        expect(market.id).toEqual(47370);
        expect(market.originId).toEqual(47370);
        expect(market.periodId).toEqual(47551);
        expect(market.type).toEqual('Handicap');
        expect(market.outcomeA.hasValue).toEqual(true);
        expect(market.outcomeA.tip).toEqual('+0.60');
        expect(market.outcomeB.hasValue).toEqual(true);
        expect(market.outcomeB.tip).toEqual('-0.60');
        expect(market.hdp).toEqual(0.6);
    });

    it('should correctly copy market', () => {
        const market = new HandicapMarket({
            id: 47370,
            periodId: 47551,
            away: 1.40,
            home: 3.01,
            hdp: -0.6
        });
        const marketCopy = new HandicapMarket(market);

        expect(marketCopy.id).toEqual(47370);
        expect(market.originId).toEqual(47370);
        expect(market.periodId).toEqual(47551);
        expect(market.type).toEqual('Handicap');
        expect(marketCopy.outcomeA.hasValue).toEqual(true);
        expect(marketCopy.outcomeA.tip).toEqual('+0.60');
        expect(marketCopy.outcomeB.hasValue).toEqual(true);
        expect(marketCopy.outcomeB.tip).toEqual('-0.60');
        expect(marketCopy.hdp).toEqual(0.6);
    });

    it('should return outcome name', () => {
        const event = new LeagueEvent({
            home: 'Chelsea',
            away: 'Ural'
        });
        const market = new HandicapMarket({
            id: 47370,
            periodId: 47551,
            away: 1.40,
            home: 3.01,
            hdp: 0.6
        });

        expect(market.getOutcomeName(OutcomeType.AWAY, event)).toEqual('Ural -0.60');
        expect(market.getOutcomeName(OutcomeType.HOME, event)).toEqual('Chelsea +0.60');
    });

    it('should return default outcome name if outcome type is undefined or wrong', () => {
        const market = new HandicapMarket();
        const event = new LeagueEvent();

        expect(market.getOutcomeName('random value', event)).toEqual('');
        expect(market.getOutcomeName('', event)).toEqual('');
    });
});