import * as React from 'react';

export class EventListBlurredItem extends React.Component<{}> {

    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <div className="a-event a-event_blur_on">
                <div className="a-event__actions">
                    <div className="a-event__actions-title">
                        <div className="a-event__empty-line"/>
                    </div>
                    <span className="a-event__id"/><span className="a-event__time"/>
                    <div className="a-event__actions-line a-event__actions-line_type_time"/>
                    <div className="a-event__actions-line a-event__actions-line_type_time"/>
                    <div className="a-event__actions-line a-event__actions-line_type_time">
                        <div className="a-event__time-value a-event__time-value_active"/>
                    </div>
                </div>
                <div className="a-event__outcomes">
                    <div className="a-event__outcomes-title">
                        <div className="a-event__empty-line"/>
                    </div>
                    <div className="a-event__outcomes-item">
                        <div className="a-event__empty-line a-event__empty-line_size_big"/>
                    </div>
                    <div className="a-event__outcomes-item">
                        <div className="a-event__empty-line a-event__empty-line_size_big a-event__empty-line_num_2"/>
                    </div>
                    <div className="a-event__outcomes-item">
                        <div className="a-event__empty-line a-event__empty-line_size_big"/>
                    </div>
                </div>
                <div className="a-event-market a-event-market_type_ml">
                    <div className="a-event-market__title">
                        <div className="a-event-market__empty-line"/>
                    </div>
                    <div className="a-event-market__line">
                        <div className="a-event-market__outcome-value"/>
                    </div>
                    <div className="a-event-market__line">
                        <div className="a-event-market__outcome-value"/>
                    </div>
                    <div className="a-event-market__line">
                        <div className="a-event-market__outcome-value"/>
                    </div>
                </div>
                <div className="a-event-market a-event-market_type_dc">
                    <div className="a-event-market__title">
                        <div className="a-event-market__empty-line"/>
                    </div>
                    <div className="a-event-market__line">
                        <div className="a-event-market__outcome-subtitle">
                            <div className="a-event-market__empty-line"/>
                        </div>
                        <div className="a-event-market__outcome-value"/>
                    </div>
                    <div className="a-event-market__line">
                        <div className="a-event-market__outcome-subtitle">
                            <div className="a-event-market__empty-line"/>
                        </div>
                        <div className="a-event-market__outcome-value"/>
                    </div>
                    <div className="a-event-market__line">
                        <div className="a-event-market__outcome-subtitle">
                            <div className="a-event-market__empty-line"/>
                        </div>
                        <div className="a-event-market__outcome-value"/>
                    </div>
                </div>
                <div className="a-event-market a-event-market_type_handicap">
                    <div className="a-event-market__title">
                        <div className="a-event-market__empty-line"/>
                    </div>
                    <div className="a-event-market__line">
                        <div className="a-dropdown-twin-select"/>
                    </div>
                    <div className="a-event-market__line"/>
                    <div className="a-event-market__line">
                        <div className="a-dropdown-twin-select"/>
                    </div>
                </div>
                <div className="a-event-market a-event-market_type_totals">
                    <div className="a-event-market__title">
                        <div className="a-event-market__empty-line"/>
                    </div>
                    <div className="a-event-market__line">
                        <div className="a-dropdown-twin-select"/>
                    </div>
                    <div className="a-event-market__line a-event-market__line_content-pos_left">
                        <div className="a-event-market__individual-total">
                            <div className="a-event-market__empty-line"/>
                        </div>
                        <div className="a-event-market__individual-total">
                            <div className="a-event-market__empty-line"/>
                        </div>
                    </div>
                    <div className="a-event-market__line">
                        <div className="a-dropdown-twin-select"/>
                    </div>
                </div>
                <div className="a-event-market a-event-market_type_bts">
                    <div className="a-event-market__title">
                        <div className="a-event-market__empty-line"/>
                    </div>
                    <div className="a-event-market__line">
                        <div className="a-event-market__outcome-subtitle">
                            <div className="a-event-market__empty-line"/>
                        </div>
                        <div className="a-event-market__outcome-value a-event-market__outcome-value_type_no-line"/>
                    </div>
                    <div className="a-event-market__line"/>
                    <div className="a-event-market__line">
                        <div className="a-event-market__outcome-subtitle">
                            <div className="a-event-market__empty-line"/>
                        </div>
                        <div className="a-event-market__outcome-value a-event-market__outcome-value_type_no-line"/>
                    </div>
                </div>
            </div>
        );
    }
}
