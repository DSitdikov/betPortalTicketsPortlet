import { ActionCreator, Dispatch } from 'redux';
import { UiActionType } from '../ui-action-type';
import FastDealData from '../../../entities/ui/modals/fast-deal/fast-deal-data';
import CommonAction from '../../common/types/common-action';
import { ThunkAction } from 'redux-thunk';
import StoreState from '../../../entities/store-state';
import { TotalType } from '../../../entities/domain-model/markets/total-market/total-type';
import Market from '../../../entities/domain-model/markets/common/market';
import LeagueEventPeriod from '../../../entities/domain-model/league-event-period/league-event-period';
import { showModal } from '../modal/modal.actions';
import { ModalType } from '../../../enums/modal-type';
import PeriodUiState from '../../../entities/ui/period-ui-state/period-ui-state';

export const selectMarket: ActionCreator<ThunkAction<CommonAction, StoreState, void>> = (marketId: number) => {
    return (dispatch: Dispatch<StoreState>, getState: () => StoreState): CommonAction => {
        const state = getState();
        const periods = state.entities.periods;
        const markets = state.entities.markets;

        const market = markets.get(marketId) as Market;
        const period = periods.get(market.periodId) as LeagueEventPeriod;
        const periodUiState = state.ui.periods.get(market.periodId) as PeriodUiState;

        const selectedTotalsType = periodUiState.selectedTotalsType;
        const periodMarkets = [
            ...period.handicap,
            ...period.allTotals[selectedTotalsType || TotalType.OVERALL]
        ];
        const marketSiblingIds = periodMarkets.filter(id => (markets.get(id) as Market).type === market.type);
        return dispatch({
            type: UiActionType.SELECT_MARKET,
            payload: {
                marketId,
                marketSiblingIds
            }
        });
    };
};

export const toggleTotalType: ActionCreator<CommonAction> = (periodId: number, totalType: number) => ({
    type: UiActionType.TOGGLE_TOTAL_TYPE,
    payload: {
        periodId,
        totalType
    }
});

export const selectLine: ActionCreator<ThunkAction<CommonAction, StoreState, void>> = (data: FastDealData) => {
    return (dispatch: Dispatch<StoreState>): CommonAction => {
        dispatch(showModal(ModalType.BET));
        return dispatch(updateFastDealData(data));
    };
};

export const updateFastDealData: ActionCreator<CommonAction> = (data: FastDealData) => ({
    type: UiActionType.UPDATE_FAST_DEAL_DATA,
    payload: data
});
