import { combineReducers } from 'redux';
import uiReducer from './ui/ui.reducer';
import entitiesReducer from './entities/entities.reducer';
import StoreState from '../entities/store-state';

const rootReducer = combineReducers<StoreState>({
    ui: uiReducer,
    entities: entitiesReducer
});

export default rootReducer;