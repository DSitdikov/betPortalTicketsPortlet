import { ThunkAction } from 'redux-thunk';
import { Action, ActionCreator, Dispatch } from 'redux';
import StoreState from '../../../entities/store-state';
import { fetchTicketsSuccess, fetchLeaguesSuccess } from '../../';
import { setBusy, showError } from '../../ui/common/common.actions';
import InitialDataLoader from '../../../services/data-loaders/initial-data-loader/initial-data-loader';

export const loadInitialData: ActionCreator<ThunkAction<Promise<Action>, StoreState, void>> = () => {
    return (dispatch: Dispatch<StoreState>, getState: () => StoreState): Promise<Action> => {
        const customerId = getState().entities.customer.id;
        const sportId = getState().entities.sport.id;
        const dataLoader = new InitialDataLoader();
        dispatch(setBusy(true));
        return dataLoader.loadData(customerId, sportId)
            .then((results) => {
                const [leagueEntities, tickets] = results;
                dispatch(fetchLeaguesSuccess(leagueEntities));
                dispatch(fetchTicketsSuccess(tickets));
            })
            .catch(error => dispatch(showError(error)))
            .then(() => dispatch(setBusy(false)));
    };
};