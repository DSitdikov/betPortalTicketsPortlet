import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import StoreState from '../entities/store-state';
import AppError from '../exceptions/app-error';
import Notification from './common/notification';
import ClientArea from './client-area/client-area';
import LoginView from './login/login-view';
import NotFound from './common/not-found';
import RouteGuarded from './common/route-guarded';
import ModalLoader from './modals/modal-loader';
import ModalNotification from './modals/modal-notification';
import ModalFastDealBet from './modals/modal-fast-deal-bet';
import ModalFastDealConfirm from './modals/modal-fast-deal-confirm';
import classNames from 'classnames';
import { ModalType } from '../enums/modal-type';
import AppNotification from '../entities/ui/app-notification/app-notification';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { initCustomer } from '../actions';
import CommonAction from '../actions/common/types/common-action';
import { Action } from 'redux';

interface StateToPropsType {
    busy: boolean;
    error: AppError | undefined;
    notification: AppNotification | undefined;
    modal: ModalType;
}

interface DispathToPropsType {
    initCustomer: () => Action;
}

type Props = StateToPropsType & DispathToPropsType;

class App extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    componentWillMount() {
        this.props.initCustomer();
    }

    render() {
        const notification = this.props.notification || {} as AppNotification;
        const showNotification = Boolean(notification.type);
        const isShowOverlay = this.props.busy
            || this.props.error
            || this.props.modal !== ModalType.NONE;
        const isLoginForm = window.location.pathname.includes('login');

        const overlayClass = classNames({
            'a-overlay': true,
            'a-overlay_blur_on': isShowOverlay,
            'a-overlay_mode_login-form': isLoginForm
        });

        return (
            <div className="a-page">
                <div className={overlayClass}>
                    <Switch>
                        <RouteGuarded path="/client" component={ClientArea}/>
                        <Route path="/login" component={LoginView}/>
                        <Route path="/404" component={NotFound}/>
                        <Redirect from={'/'} to={'/client'}/>
                        <Redirect from={'*'} to={'/404'}/>
                    </Switch>
                    <Notification
                        title={notification.title}
                        message={notification.message}
                        mode={notification.type}
                        show={showNotification}
                    />
                </div>
                <ModalNotification/>
                <ModalFastDealBet/>
                <ModalFastDealConfirm/>
                <ModalLoader/>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        busy: state.ui.common.busy,
        error: state.ui.common.error,
        notification: state.ui.common.notification,
        modal: state.ui.modal
    };
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        initCustomer: () => dispatch<any>(initCustomer())
    };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
