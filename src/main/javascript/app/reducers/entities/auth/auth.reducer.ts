import AuthUiState from '../../../entities/ui/auth-state/auth-ui.state';
import CommonAction from '../../../actions/common/types/common-action';
import { DomainModelActionType } from '../../../actions/domain-model/domain-model-action-type';

export default function authReducer(state: AuthUiState = new AuthUiState(), action: CommonAction) {
    switch (action.type) {
        case DomainModelActionType.LOGIN_SUCCESS: {
            const resultState = new AuthUiState(state);
            resultState.busy = false;
            resultState.logged = true;
            resultState.error = undefined;
            return resultState;
        }
        case DomainModelActionType.LOGIN_REQUEST: {
            const resultState = new AuthUiState(state);
            resultState.busy = true;
            resultState.error = undefined;
            return resultState;
        }
        case DomainModelActionType.LOGIN_FAILURE: {
            const resultState = new AuthUiState(state);
            resultState.busy = false;
            resultState.error = action.payload;
            return resultState;
        }
        case DomainModelActionType.LOGOUT_SUCCESS: {
            return new AuthUiState();
        }
        default:
            return state;
    }
}
