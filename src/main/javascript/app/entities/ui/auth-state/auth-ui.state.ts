import AppError from '../../../exceptions/app-error';

export default class AuthUiState {
    busy: boolean;
    error?: AppError;
    logged?: boolean;

    constructor(data: any = {}) {
        this.busy = data.busy || false;
        this.error = data.error;
        this.logged = data.logged || false;
    }
}
