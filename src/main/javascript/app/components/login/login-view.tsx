import * as React from 'react';
import { Action } from 'redux';
import * as actions from '../../actions';
import { connect, Dispatch } from 'react-redux';
import StoreState from '../../entities/store-state';
import AuthData from '../../entities/domain-model/auth/auth-data';
import Btn from '../common/btn';
import { Redirect } from 'react-router-dom';
import classNames from 'classnames';
import AppError from '../../exceptions/app-error';
import { messages } from '../../messages/messages';
import CommonAction from '../../actions/common/types/common-action';

interface DispathToPropsType {
    login: (data: AuthData) => Action;
}

interface StateToPropsType {
    logged?: boolean;
    error?: AppError;
    busy: boolean;
}

interface State {
    login: string;
    password: string;
    rememberMe: boolean;
}

interface Props extends DispathToPropsType, StateToPropsType {
    location: { state: any };
}

class LoginView extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            login: '',
            password: '',
            rememberMe: false
        };
    }

    handleSwitcherState() {
        this.setState({
            rememberMe: !this.state.rememberMe
        });
    }

    handleLogin() {
        if (!this.isInputCorrect()) {
            return;
        }

        const authData = new AuthData({
            login: this.state.login,
            password: this.state.password,
            rememberMe: this.state.rememberMe
        });

        this.props.login(authData);
    }

    handleLoginChange<P>(e: React.FormEvent<P>) {
        const target = e.target as HTMLFormElement;
        this.setState({
            login: target.value
        });
    }

    handlePasswordChange<P>(e: React.FormEvent<P>) {
        const target = e.target as HTMLFormElement;
        this.setState({
            password: target.value
        });
    }

    isInputCorrect() {
        return 0 < this.state.login.length && 0 < this.state.password.length;
    }

    render() {
        const state = this.props.location.state || {};
        const from = state.from || '/client';
        const hasError = !!this.props.error;

        const switcherClasses = classNames({
            'a-switcher': true,
            'a-switcher_mode_on': this.state.rememberMe
        });

        const alertClasses = classNames({
            'a-login-form__alert': true,
            'a-login-form__alert_showed': hasError
        });

        if (this.props.logged) {
            return (
                <Redirect to={from}/>
            );
        }

        const errorCode = this.props.error ? this.props.error.code : 'unknown';
        let errorMessage = 'Unknown error';
        if (this.props.error && messages.en.auth.error[this.props.error.message]) {
            errorMessage = messages.en.auth.error[this.props.error.message];
        }

        return (
            <form className="a-login-form">
                <div className="a-login-form__heading">Log In</div>
                <div className="a-login-form__description">
                    Please enter your personal data for authorization.
                </div>
                <input
                    onChange={e => this.handleLoginChange(e)}
                    value={this.state.login}
                    placeholder="Username"
                    className="a-login-form__input"
                />
                <input
                    onChange={e => this.handlePasswordChange(e)}
                    value={this.state.password}
                    placeholder="Password"
                    type="password"
                    className="a-login-form__input a-login-form__input_type_password"
                />
                <div className="a-login-form__remember-me">
                    <div className="a-login-form__remember-me-title">Remember me</div>
                    <div className="a-login-form__remember-me-switcher">
                        <div className={switcherClasses} onClick={() => this.handleSwitcherState()}>
                            <div className="a-switcher__handle"/>
                        </div>
                    </div>
                </div>
                <div className={alertClasses}>
                    {errorMessage}
                    <br/>
                    (Error code: {errorCode})
                </div>
                <div className="a-login-form__btn-submit">
                    <Btn
                        title={'Log in'}
                        disabled={!this.isInputCorrect()}
                        busy={this.props.busy}
                        handleAction={() => this.handleLogin()}
                    />
                </div>
            </form>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        logged: state.entities.authentication.logged,
        busy: state.ui.authentication.busy,
        error: state.ui.authentication.error
    };
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        login: (data: AuthData) => dispatch<any>(actions.login(data))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginView);
