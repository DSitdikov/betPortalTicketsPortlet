import * as React from 'react';
import { Action } from 'redux';
import { logout } from '../../actions';
import CommonAction from '../../actions/common/types/common-action';
import { connect, Dispatch } from 'react-redux';

interface DispathToPropsType {
    logout: () => Action;
}

class Navbar extends React.Component<DispathToPropsType> {

    constructor(props: DispathToPropsType) {
        super(props);
    }

    render() {
        return (
            <div className="a-navbar">
                <div className="a-navbar__left-side"/>
                <div className="a-menu">
                    <span className="a-menu__item a-menu__item_selected" title="Events">Events</span>
                    <a className="a-menu__item a-menu__item_pos_right"/>
                </div>
                <div className="a-navbar__right-side">
                    <div className="a-navbar__btn-logout" title="Sign out" onClick={() => this.props.logout()}>
                        <div className="a-vip-icon a-vip-icon_logout"/>
                    </div>
                </div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        logout: () => dispatch<any>(logout())
    };
}

export default connect(null, mapDispatchToProps)(Navbar);
