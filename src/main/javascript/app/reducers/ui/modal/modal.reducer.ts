import CommonAction from '../../../actions/common/types/common-action';
import { ModalType } from '../../../enums/modal-type';
import { UiActionType } from '../../../actions/ui/ui-action-type';

export default function modalReducer(state: ModalType = ModalType.NONE, action: CommonAction) {
    switch (action.type) {
        case UiActionType.SHOW_MODAL:
            return action.payload as ModalType;
        case UiActionType.CLOSE_MODAL:
            return ModalType.NONE;
        default:
            return state;
    }
}