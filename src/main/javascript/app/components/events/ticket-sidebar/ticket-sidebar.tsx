import * as React from 'react';
import { connect } from 'react-redux';
import StoreState from '../../../entities/store-state';
import Scrollbars from 'react-custom-scrollbars-patched';
import Ticket from '../../../entities/domain-model/ticket/ticket';
import TicketView from './ticket-view/ticket-view';
import ScrollComponent from '../../common/scroll-component';

interface StateToPropsType {
    tickets: Map<number, Ticket>;
}

type Props = StateToPropsType;

class TicketSidebar extends ScrollComponent<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        const tickets = [
            new Ticket({
                id: 0,
                eventName: 'Tottenham Hotspur - West Ham United',
                outcomeName: 'West Ham United M',
                priceFormatted: '1.34',
                amountFormatted: '235 000'
            }),
            new Ticket({
                id: 1,
                eventName: 'Tottenham Hotspur - West Ham United',
                outcomeName: 'Draw',
                priceFormatted: '2.12',
                amountFormatted: '230 000'
            }),
            new Ticket({
                id: 2,
                eventName: 'Tottenham Hotspur - West Ham United',
                outcomeName: 'Tottenham Hotspur M',
                priceFormatted: '3.15',
                amountFormatted: '200 000'
            }),
            new Ticket({
                id: 3,
                eventName: 'Tottenham Hotspur - West Ham United',
                outcomeName: 'Over 2.25',
                priceFormatted: '2.00',
                amountFormatted: '150 000'
            })
        ];

        return (
            <div className="a-sidebar-right">
                <div className="a-collapse-group a-collapse-group">
                    <div className="a-collapse-group__header">
                        <div className="a-collapse-group__header-counter">{tickets.length}</div>
                        <div className="a-collapse-group__header-title">Accepted tickets</div>
                        <div className="a-collapse-group__header-btn-expand">
                            <div className="a-vip-icon a-vip-icon_arrow-dropdown"/>
                        </div>
                    </div>
                    <div className="a-collapse-group__content">
                        <Scrollbars
                            className="a-scrollbar"
                            autoHide={true}
                            browserScrollbarWidth={this.state.scrollbarWidth}
                            renderView={props => <div {...props} className="a-scrollbar__container"/>}
                            renderThumbVertical={props => <div {...props} className="a-scrollbar__thumb-vertical"/>}
                        >
                            {tickets.map(ticket => (<TicketView key={ticket.id} ticket={ticket}/>))}
                        </Scrollbars>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        tickets: state.entities.tickets,
    };
}

export default connect(mapStateToProps)(TicketSidebar);
