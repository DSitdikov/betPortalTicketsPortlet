import * as React from 'react';
import { Action } from 'redux';
import { connect, Dispatch } from 'react-redux';
import classNames from 'classnames';
import League from '../../../../entities/domain-model/league/league';
import CommonAction from '../../../../actions/common/types/common-action';
import StoreState from '../../../../entities/store-state';
import LeagueEventView from './league-event-view/league-event-view';
import LeagueUiState from '../../../../entities/ui/league-ui-state/league-ui-state';
import { toggleExpandStateOfLeague, toggleSelectionOfLeague } from '../../../../actions';

interface StateToPropsType {
    leagueUiState: Map<number, LeagueUiState>;
}

interface DispathToPropsType {
    onToggleSelectionOfLeague: (id: number) => Action;
    onToggleExpandStateOfLeague: (id: number) => Action;
}

interface Props extends DispathToPropsType, StateToPropsType {
    league: League;
}

class LeagueView extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    toggleExpandState(leagueId: number, e: any) {
        this.props.onToggleExpandStateOfLeague(leagueId);
        e.stopPropagation();
    }

    render() {
        const league = this.props.league;
        const leagueUiState = this.props.leagueUiState.get(league.id) as LeagueUiState;

        const checkboxStyleClass = classNames({
            'a-checkbox': true,
            'a-checkbox_selected': leagueUiState.selected
        });

        const eventsStyleClass = classNames({
            'a-league__events': true,
            'a-league__events_expanded': leagueUiState.expanded
        });

        const btnExpandStyleClass = classNames({
            'a-league__btn-expand': true,
            'a-league__btn-expand_active': leagueUiState.expanded
        });

        return (
            <div className="a-league">
                <div className="a-league__header" onClick={() => this.props.onToggleSelectionOfLeague(league.id)}>
                    <div className="a-league__checkbox">
                        <div className={checkboxStyleClass}>
                            <div className="a-vip-icon a-vip-icon_checkmark"/>
                        </div>
                    </div>
                    <div className="a-league__title">{league.name}</div>
                    <div className={btnExpandStyleClass} onClick={(e) => this.toggleExpandState(league.id, e)}>
                        <div className="a-vip-icon a-vip-icon_arrow-dropdown"/>
                    </div>
                </div>
                <ul className={eventsStyleClass}>
                    {league.events.map(eventId => (
                        <LeagueEventView key={eventId} eventId={eventId}/>
                    ))}
                </ul>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        leagueUiState: state.ui.leagues
    };
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        onToggleSelectionOfLeague: (id: number) => dispatch<any>(toggleSelectionOfLeague(id)),
        onToggleExpandStateOfLeague: (id: number) => dispatch<any>(toggleExpandStateOfLeague(id))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LeagueView);
