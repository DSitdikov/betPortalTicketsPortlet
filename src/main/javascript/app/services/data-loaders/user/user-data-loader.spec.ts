import UserDataLoader from './user-data-loader';
import DataLoader from '../common/data-loader';
import Mock = jest.Mock;
import Customer from '../../../entities/domain-model/customer/customer';
import AppError from '../../../exceptions/app-error';

describe('UserDataLoader', () => {
    let service;

    const basicLoader = {
        get: jest.fn(),
        post: jest.fn()
    } as DataLoader;

    beforeEach(() => {
        (<Mock> basicLoader.get).mockReturnValue(Promise.resolve({
            id: 17,
            login: 'M011'
        }));

        service = new UserDataLoader({ loader: basicLoader });
    });

    describe('#getInfo', () => {
        it('should use correct url to get info', () => {
            service.getInfo();
            expect((<Mock> basicLoader.get).mock.calls[0][0]).toEqual('/info.success.json');
        });

        it('should return customer on success', () => {
            return service.getInfo()
                .then(customer => {
                    expect(customer instanceof Customer).toBeTruthy();
                    expect(customer.id).toEqual(17);
                    expect(customer.login).toEqual('M011');
                })
                .catch(error => expect(error).toBeUndefined());
        });

        it('should return error on fail', () => {
            (<Mock> basicLoader.get).mockReturnValue(Promise.reject(new AppError({ code: 1 })));
            return service.getInfo()
                .then(customer => expect(customer).toBeUndefined())
                .catch(error => expect(error.code).toEqual(1));
        });
    });
});