import * as React from 'react';
import classNames from 'classnames';
import StoreState from '../../entities/store-state';
import { connect } from 'react-redux';

interface StateToPropsType {
    busy: boolean;
}

class ModalLoader extends React.Component<StateToPropsType> {

    constructor(props: StateToPropsType) {
        super(props);
    }

    render() {
        const busy = this.props.busy;

        const modalClass = classNames({
            'a-modal': true,
            'a-modal_mode_loader': busy,
            'a-modal_showed': busy
        });

        return (
            <div className={modalClass}>
                <div className="a-modal__overlay">
                    <div className="a-modal__loader">
                        <div className="a-loader"/>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        busy: state.ui.common.busy
    };
}

export default connect(mapStateToProps)(ModalLoader);
