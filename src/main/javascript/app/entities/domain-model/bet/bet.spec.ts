import Bet from './bet';

describe('Bet', () => {
    it('should create default instance if there are no constructor parameters', () => {
        const bet = new Bet();

        expect(bet.id).toEqual(0);
        expect(bet.marketId).toEqual(0);
        expect(bet.customerId).toEqual('');
        expect(bet.typeValue).toEqual('');
        expect(bet.type).toEqual('none');
        expect(bet.amount).toEqual(0);
        expect(bet.price).toEqual(0);
    });

    it('should parse raw bet', () => {
        const bet = new Bet({
            id: '66765',
            marketId: '10901',
            amount: '500000',
            customerId: 'M177',
            price: '7.77',
            type: 'away',
            typeValue: 'some value'
        });

        expect(bet.id).toEqual(66765);
        expect(bet.marketId).toEqual(10901);
        expect(bet.customerId).toEqual('M177');
        expect(bet.typeValue).toEqual('some value');
        expect(bet.type).toEqual('away');
        expect(bet.amount).toEqual(500000);
        expect(bet.price).toEqual(7.77);
    });

    it('should clone bet correctly', () => {
        const bet = new Bet({
            id: '66765',
            marketId: '10901',
            amount: '500000',
            customerId: 'M177',
            price: '7.77',
            type: 'away',
            typeValue: 'some value'
        });
        const betClone = new Bet(bet);

        expect(bet.id).toEqual(66765);
        expect(betClone.marketId).toEqual(10901);
        expect(betClone.customerId).toEqual('M177');
        expect(betClone.typeValue).toEqual('some value');
        expect(betClone.type).toEqual('away');
        expect(betClone.amount).toEqual(500000);
        expect(betClone.price).toEqual(7.77);
    });
});