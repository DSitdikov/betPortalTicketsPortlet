import * as React from 'react';
import Tabs from './tabs/tabs';
import EventList from './event-list/event-list';

type Props = {};

export default class EventsContent extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <div className="a-content">
                <Tabs/>
                <EventList/>
            </div>
        );
    }
}