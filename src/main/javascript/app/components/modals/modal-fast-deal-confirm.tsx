import * as React from 'react';
import FastDealData from '../../entities/ui/modals/fast-deal/fast-deal-data';
import NumberFormatUtils from '../../utils/number-format-utils';
import { Action } from 'redux';
import { connect, Dispatch } from 'react-redux';
import StoreState from '../../entities/store-state';
import classNames from 'classnames';
import CommonAction from '../../actions/common/types/common-action';
import { addTicket } from '../../actions';
import { closeModal } from '../../actions/ui/modal/modal.actions';
import League from '../../entities/domain-model/league/league';
import { ModalType } from '../../enums/modal-type';
import LeagueEvent from '../../entities/domain-model/league-event/league-event';

interface StateToPropsType {
    leagues: Map<number, League>;
    events: Map<number, LeagueEvent>;
    data: FastDealData;
    opened: boolean;
}

interface DispathToPropsType {
    onCloseModal: () => Action;
    onConfirm: (data: FastDealData) => Promise<Action>;
}

interface Props extends StateToPropsType, DispathToPropsType {
    data: FastDealData;
}

interface BetValueState {
    betValue: number;
}

class ModalFastDealConfirm extends React.Component<Props, BetValueState> {

    constructor(props: Props) {
        super(props);

        this.state = {
            betValue: 50000
        };
    }

    render() {
        const { data, leagues, events } = this.props;
        const event = events.get(data.event) as LeagueEvent || new LeagueEvent();
        const league = leagues.get(event.leagueId);
        const leagueName = league ? league.name : '';

        const modalClass = classNames({
            'a-modal': true,
            'a-modal_mode_fast-deal-confirm': true,
            'a-modal_showed': this.props.opened
        });

        return (
            <div className={modalClass}>
                <div className="a-modal__overlay">
                    <div className="a-modal__window a-modal__window_type_fast-deal-confirm">
                        <div className="a-modal-deal">
                            <div className="a-modal-deal__sidebar">
                                <div className="a-modal-deal__sidebar-row a-modal-deal__sidebar-row_type_header">
                                    <div className="a-modal-deal__league">
                                        <div className="a-modal-deal__league-logo"/>
                                        <div className="a-modal-deal__league-name">
                                            {leagueName}
                                        </div>
                                        <div className="a-modal-deal__league-country hidden">England</div>
                                    </div>
                                    <div className="a-modal-deal__icon">
                                        <div className="a-vip-icon a-vip-icon_zip"/>
                                    </div>
                                </div>
                                <div className="a-modal-deal__sidebar-row">
                                    <div className="a-modal-deal__team">
                                        <div className="a-modal-deal__team-logo"/>
                                        <div className="a-modal-deal__team-title">
                                            {event.home}
                                        </div>
                                    </div>
                                    <div className="a-modal-deal__team">
                                        <div className="a-modal-deal__team-logo"/>
                                        <div className="a-modal-deal__team-title">
                                            {event.away}
                                        </div>
                                    </div>
                                </div>
                                <div className="a-modal-deal__sidebar-row">
                                    <div className="a-modal-deal__date">
                                        {event.startDateFormatted}
                                    </div>
                                    <div className="a-modal-deal__start-time">
                                        {event.startIn}
                                    </div>
                                </div>
                            </div>
                            <div className="a-modal-deal__content">
                                <div className="a-modal-deal__header">
                                    <div className="a-modal-deal__menu">
                                        <div className="a-modal-deal__menu-item a-modal-deal__menu-item_selected">
                                            Fast deal
                                        </div>
                                        <div className="a-modal-deal__menu-item a-modal-deal__menu-item_disabled">
                                            Quote
                                        </div>
                                        <div className="a-modal-deal__menu-item a-modal-deal__menu-item_disabled">
                                            Order
                                        </div>
                                        <div className="a-modal-deal__menu-item a-modal-deal__menu-item_disabled">
                                            Multi Order
                                        </div>
                                    </div>
                                </div>
                                <table className="a-modal-deal__table">
                                    <thead className="a-modal-deal__table-head">
                                    <tr className="a-modal-deal__table-head-row">
                                        <th className="a-modal-deal__table-head-item"/>
                                        <th className="a-modal-deal__table-head-item">Line</th>
                                        <th className="a-modal-deal__table-head-item">To Bet</th>
                                    </tr>
                                    </thead>
                                    <tbody className="a-modal-deal__table-body">
                                    <tr className="a-modal-deal__table-body-row">
                                        <td className="a-modal-deal__table-body-item">
                                            <div className="a-modal-deal__event">
                                                {event.name}
                                            </div>
                                        </td>
                                        <td className="a-modal-deal__table-body-item"/>
                                        <td className="a-modal-deal__table-body-item"/>
                                    </tr>
                                    <tr className="a-modal-deal__table-body-row">
                                        <td className="a-modal-deal__table-body-item">
                                            {data.outcomeName}
                                        </td>
                                        <td
                                            className={'a-modal-deal__table-body-item ' +
                                            'a-modal-deal__table-body-item_text-pos_center'}
                                        >
                                            {data.price.formatted}
                                        </td>
                                        <td
                                            className={'a-modal-deal__table-body-item ' +
                                            'a-modal-deal__table-body-item_text-pos_center'}
                                        >
                                            {NumberFormatUtils.isolateThousands(data.betValue)}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div className="a-modal-deal__clear"/>
                                <div className="a-modal-deal__buttons">
                                    <div className="a-modal-deal__btn" onClick={() => this.props.onCloseModal()}>Cancel
                                    </div>
                                    <div
                                        className={
                                            'a-modal-deal__btn ' +
                                            'a-modal-deal__btn_type_fill ' +
                                            'a-modal-deal__btn_pos_right'
                                        }
                                        onClick={() => this.props.onConfirm(data)}
                                    >
                                        Confirm
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        leagues: state.entities.leagues,
        events: state.entities.events,
        data: state.ui.fastDealData,
        opened: state.ui.modal === ModalType.BET_CONFIRM
    };
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        onCloseModal: () => dispatch(closeModal()),
        onConfirm: (data: FastDealData) => dispatch<any>(addTicket(data))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalFastDealConfirm);
