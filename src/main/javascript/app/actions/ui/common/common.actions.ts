import { Action, ActionCreator } from 'redux';
import { UiActionType } from '../ui-action-type';
import AppError from '../../../exceptions/app-error';
import CommonAction from '../../';
import AppNotification from '../../../entities/ui/app-notification/app-notification';
import { AppNotificationType } from '../../../entities/ui/app-notification/app-notification-type';

export const setBusy: ActionCreator<CommonAction> = (busy: boolean) => ({
    type: UiActionType.SET_BUSY,
    payload: busy
});

export const showError: ActionCreator<CommonAction> = (error: AppError) => ({
    type: UiActionType.SHOW_ERROR,
    payload: error
});

export const showErrorNotification: ActionCreator<CommonAction> = () => ({
    type: UiActionType.SHOW_NOTIFICATION,
    payload: new AppNotification({
        type: AppNotificationType.DANGER,
        title: 'Error!',
        message: 'Something went wrong'
    })
});

export const closeNotification: ActionCreator<Action> = () => ({
    type: UiActionType.CLOSE_NOTIFICATION
});