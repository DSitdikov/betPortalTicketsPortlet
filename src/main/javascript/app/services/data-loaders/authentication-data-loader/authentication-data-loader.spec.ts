import AuthenticationDataLoader from './authentication-data-loader';
import DataLoader from '../common/data-loader';
import Mock = jest.Mock;

describe('AuthenticationDataLoader', () => {
    let service;

    const basicLoader = {
        get: jest.fn(),
        post: jest.fn()
    } as DataLoader;

    beforeEach(() => {
        service = new AuthenticationDataLoader({ loader: basicLoader });
    });

    describe('#login', () => {
        const AUTH_DATA = { login: 'F901', password: 'securityvipbet' };

        beforeEach(() => {
            (<Mock> basicLoader.post).mockReturnValue(Promise.resolve({ id: 'F901', login: 'F901' }));
        });

        it('should pass correct url and data to data loader', () => {
            service.login(AUTH_DATA);

            const callParams = (<Mock> basicLoader.post).mock.calls[0];

            expect(callParams[0]).toEqual('/login.success.json');
            expect(callParams[1]).toEqual(AUTH_DATA);
        });

        it('should return customer if auth is succeed', () => {
            return service.login(AUTH_DATA)
                .then(customer => expect(customer.login).toEqual('F901'))
                .catch(error => expect(error).toBeUndefined());
        });

        it('should return error if auth is fail', () => {
            (<Mock> basicLoader.post).mockReturnValue(Promise.reject({ code: 101401 }));

            return service.login(AUTH_DATA)
                .then(customer => expect(customer).toBeUndefined())
                .catch(error => expect(error.code).toEqual(101401));
        });
    });

    describe('#logout', () => {
        beforeEach(() => {
            (<Mock> basicLoader.post).mockReturnValue(Promise.resolve(undefined));
        });

        it('should pass correct url to data loader', () => {
            service.logout();

            const callParams = (<Mock> basicLoader.post).mock.calls[0];
            expect(callParams[0]).toEqual('/logout.success.json');
        });

        it('should return customer if auth is succeed', () => {
            return service.logout()
                .then(() => expect('do nothing').toBeTruthy())
                .catch(error => expect(error).toBeUndefined());
        });

        it('should return error if auth is fail', () => {
            (<Mock> basicLoader.post).mockReturnValue(Promise.reject({ code: 902403 }));

            return service.logout()
                .then(() => expect('do nothing').toBeUndefined())
                .catch(error => expect(error.code).toEqual(902403));
        });
    });
});