export default class League {
    id: number;
    name: string;
    events: number[];

    constructor(data: any = {}) {
        this.id = data.id || 0;
        this.name = data.name || '';
        this.events = (data.events || []).map((event: any) => event.id);
    }
}