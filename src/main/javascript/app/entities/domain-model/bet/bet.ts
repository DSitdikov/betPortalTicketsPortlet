import { OutcomeType } from '../outcome/outcome-type';

export default class Bet {
    id: number;
    marketId: number;
    amount: number;
    customerId: string;
    price: number;
    type: string;
    typeValue: string;

    constructor(data?: any) {
        data = data || {};

        this.id = Number(data.id || 0);
        this.marketId = Number(data.marketId || 0);
        this.amount = Number(data.amount || 0);
        this.customerId = data.customerId || '';
        this.price = Number(data.price || 0);
        this.type = data.type || OutcomeType.NONE;
        this.typeValue = data.typeValue || '';
    }
}