import * as React from 'react';
import classNames from 'classnames';
import StoreState from '../../entities/store-state';
import { connect, Dispatch } from 'react-redux';
import { Action } from 'redux';
import CommonAction from '../../actions/common/types/common-action';
import { closeModal } from '../../actions/ui/modal/modal.actions';
import AppError from '../../exceptions/app-error';

interface DispathToPropsType {
    onCloseModal: () => Action;
}

interface StateToPropsType {
    data?: AppError;
}

type PropsType = StateToPropsType & DispathToPropsType;

class ModalNotification extends React.Component<PropsType> {

    constructor(props: PropsType) {
        super(props);
    }

    // noinspection JSMethodCanBeStatic
    reload() {
        window.location.reload();
    }

    render() {
        const modalClass = classNames({
            'a-modal': true,
            'a-modal_mode_notification': true,
            'a-modal_showed': Boolean(this.props.data)
        });

        return (
            <div className={modalClass}>
                <div className="a-modal__overlay">
                    <div className="a-modal__window a-modal__window_type_notification">
                        <div className="a-modal-notification">
                            <div className="a-modal-notification__btn-close">
                                <div className="a-vip-icon a-vip-icon_close"/>
                            </div>
                            <div className="a-modal-notification__icon">
                                <div className="a-vip-icon a-vip-icon_delete-line"/>
                            </div>
                            <div className="a-modal-notification__title">Something went wrong!</div>
                            <div className="a-modal-notification__message">
                                Unknow error with displaying this webpage.
                                To continue, press Reload or to go to another page.
                            </div>
                            <div className="a-modal-notification__btn-action" onClick={() => this.reload()}>Reload</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        data: state.ui.common.error
    };
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        onCloseModal: () => dispatch(closeModal())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalNotification);