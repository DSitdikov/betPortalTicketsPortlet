export const messagesEn = {
    events: {
        notifications: {
            success: {
                makeBet: 'Bet has been added successfully.'
            },
            error: {
                makeBet: [
                    'Unfortunately an error has been occurred during saving bet.',
                    'Try again or make a request to support.'
                ].join(' ')
            }
        }
    },
    auth: {
        error: {
            INCORRECT_LOGIN_OR_PASSWORD: 'Incorrect login or password'
        }
    }
};