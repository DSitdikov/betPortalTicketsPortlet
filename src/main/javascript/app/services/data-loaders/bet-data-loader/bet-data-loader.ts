import env from '../../../env/env';
import BasicDataLoader from '../basic-data-loader/basic-data-loader';
import Bet from '../../../entities/domain-model/bet/bet';
import FastDealData from '../../../entities/ui/modals/fast-deal/fast-deal-data';
import DataLoader from '../common/data-loader';

export default class BetDataLoader {
    loader: DataLoader;

    constructor(data: any = {}) {
        this.loader = data.loader || new BasicDataLoader({ baseApiUrlSuffix: env.apiUrls.bet.base });
    }

    addBet(customerId: string, data: FastDealData): Promise<Bet> {
        const bet = {
            marketId: data.marketId,
            amount: data.betValue,
            customerId: customerId,
            price: data.price.value,
            type: data.outcomeType,
        };

        return this.loader.post(env.apiUrls.bet.add, bet)
            .then(rawBet => new Bet(rawBet));
    }

    getBetList(customerId: string): Promise<Bet[]> {
        return this.loader.get(env.apiUrls.bet.list)
            .then((rawBets) => {
                const bets = (rawBets || []).map(item => {
                    const bet = new Bet(item);
                    bet.customerId = customerId;
                    return bet;
                });
                // Order by id DESC
                bets.sort((a, b) => b.id - a.id);
                return bets;
            });
    }
}