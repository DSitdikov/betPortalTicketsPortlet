import BasicDataLoader from '../basic-data-loader/basic-data-loader';
import Customer from '../../../entities/domain-model/customer/customer';
import env from '../../../env/env';
import DataLoader from '../common/data-loader';
import CookieUtils from '../../../utils/cookie/cookie-utils';

export default class UserDataLoader {
    loader: DataLoader;

    static getInfoFromStorage(): Customer|undefined {
        const customerData = CookieUtils.getCookie(env.userCookieName);
        return customerData ? new Customer(JSON.parse(customerData)) : undefined;
    }

    constructor(data: any = {}) {
        this.loader = data.loader || new BasicDataLoader({ baseApiUrlSuffix: env.apiUrls.user.base });
    }

    getInfo(): Promise<Customer> {
        const url = env.apiUrls.user.info;
        return this.loader.get(url)
            .then(rawCustomer => {
                return new Customer(rawCustomer);
            });
    }
}
