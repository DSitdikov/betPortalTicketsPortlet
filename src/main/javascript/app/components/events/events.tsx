import * as React from 'react';
import LeagueSidebar from './league-sidebar/league-sidebar';
import EventsContent from './event-content/events-content';
import TicketSidebar from './ticket-sidebar/ticket-sidebar';
import CommonAction from '../../actions/common/types/common-action';
import { connect, Dispatch } from 'react-redux';
import { loadInitialData, reloadData } from '../../actions';
import { Action } from 'redux';
import StoreState from '../../entities/store-state';
import env from '../../env/env';
import { ThunkAction } from 'redux-thunk';

interface DispathToPropsType {
    init: () => Action;
    reload: (callback: any) => ThunkAction<Promise<Action>, StoreState, void>;
}

type Props = DispathToPropsType;

class Events extends React.Component<Props> {
    refreshDataTimeoutId: number;

    constructor(props: Props) {
        super(props);
        this.props.init();
        this.refreshDataTimeoutId = 0;
    }

    componentWillMount() {
        this.refreshData();
    }

    componentWillUnmount() {
        window.clearTimeout(this.refreshDataTimeoutId);
    }

    refreshData() {
        this.refreshDataTimeoutId = window.setTimeout(
            () => this.props.reload(this.refreshData.bind(this)),
            env.refreshInterval
        );
    }

    render() {
        return (
            <div className="a-page__container">
                <LeagueSidebar/>
                <EventsContent/>
                <TicketSidebar/>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        init: () => dispatch<any>(loadInitialData()),
        reload: (callback: any) => dispatch<any>(reloadData()).then(() => callback())
    };
}

export default connect(null, mapDispatchToProps)(Events);
